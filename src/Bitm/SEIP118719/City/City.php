<?php

namespace App\Bitm\SEIP118719\City;
use \App\Bitm\SEIP118719\Utility\Utility;

class City {
    //public $serial_no="";
    public $id="";
    public $name="";
    public $city="";
    //public $created = "";
    ///public $modified = "";
    // public $created_by = "";
    // public $modified_by = "";
    // public $deleted_at = ""; //soft delete
    
    
    //Start Construct function. It will generate when we will create an object.
    public function __construct($data = false){
        
        if( is_array($data) && array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        
        $this->name=$data['name'];
        $this->city=$data['city'];
    }
    
     //Start Show/View Function
        public function show($id=false){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");

        $query = "SELECT * FROM `tbl_citys` WHERE id =".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_assoc($result);
        
        return $row;
    }
    //End Show/View Function
    
    //Start Index Function
    public function index(){
        $city_objs = array();
        $conn=  mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk=  mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");
        $query="SELECT * FROM `tbl_citys`";
        $result=  mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $city_objs[]=$row;
        }
        return $city_objs;
    }
    //End Index Function
    
    //Start Store Function
    public function store(){
        $conn=  mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk=  mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");
        $query="INSERT INTO `db_atomic_project_suraiya`.`tbl_citys` (`name`, `city`) VALUES ('".$this->name."', '".$this->city."')";
        $result=  mysql_query($query);
        if($result){
        Utility::message("City Name is added successfully.");
        }
            else{
               Utility::message("There is an error while saving data, please try again later..."); 
            }
        Utility::redirect('../../SEIP118719/City/index.php');
    }
    //End Store Function
  
    //Start Delete Function
    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");

        $query = "DELETE FROM `db_atomic_project_suraiya`.`tbl_citys` WHERE `tbl_citys`.`id` = ".$id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("City Name is deleted successfully.");
        }else{
            Utility::message(" Cannot delete.");
        }
        
        Utility::redirect('index.php');
    }
    //End Delete Function
    
    //Start Update Function
        public function update(){
            
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");
        
        $query = "UPDATE `db_atomic_project_suraiya`.`tbl_citys` SET `name` = '".$this->name."', `city` = '".$this->city."' WHERE `tbl_citys`.`id` = ".$this->id;

        $result = mysql_query($query);
               
        if($result){
            Utility::message("City is edited successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
     //End Update Function
}
