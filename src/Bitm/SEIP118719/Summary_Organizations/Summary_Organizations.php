<?php

namespace App\Bitm\SEIP118719\Summary_Organizations;
use \App\Bitm\SEIP118719\Utility\Utility;

class Summary_Organizations {
    //public $serial_no="";
    public $id="";
    public $company_name="";
    public $summary_organizations="";
    //public $created;
    //public $modified;
   // public $created_by;
    //public $modified_by;
    //public $deleted_at;
    
    public function __construct($data=false){
             
        if( is_array($data) && array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        $this->company_name=$data['company_name'];
        $this->summary_organizations=$data['summary_organizations'];
    }
         //Start Show/View Function
        public function show($id=false){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");

        $query = "SELECT * FROM `tbl_summary_organizations` WHERE id =".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_object($result);
        
        return $row;
    }
    //End Show/View Function
    public function index(){
        $summary_orga_objs = array();
        $conn=  mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk=  mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");
        $query="SELECT * FROM `tbl_summary_organizations`";
        $result=  mysql_query($query);
        while ($row = mysql_fetch_object($result)) {
            $summary_orga_objs[]=$row;
        }
        return $summary_orga_objs;
    }
    public function store(){
        $conn=  mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk=  mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");
        $query="INSERT INTO `db_atomic_project_suraiya`.`tbl_summary_organizations` (`company_name`, `summary_organizations`) VALUES ('".$this->company_name."', '".$this->summary_organizations."')";
        $result=  mysql_query($query);
     if($result){
        Utility::message("Organization Summary added successfully.");
        }
            else{
               Utility::message("There is an error while saving data, please try again later..."); 
            }
        Utility::redirect('index.php');
    }

    //Start Delete Function
    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");

        $query = "DELETE FROM `db_atomic_project_suraiya`.`tbl_summary_organizations` WHERE `tbl_summary_organizations`.`id` = ".$id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Summary Organization is deleted successfully.");
        }else{
            Utility::message(" Cannot delete.");
        }
        
        Utility::redirect('index.php');
    }
    //End Delete Function
      //Start Update Function
        public function update(){
            
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");
        
        $query = "UPDATE `db_atomic_project_suraiya`.`tbl_summary_organizations` SET `company_name` = '".$this->company_name."', `summary_organizations` = '".$this->summary_organizations."' WHERE `tbl_summary_organizations`.`id` = ".$this->id;

        $result = mysql_query($query);
               
        if($result){
            Utility::message("Summary of Organization is edited successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
     //End Update Function
}
