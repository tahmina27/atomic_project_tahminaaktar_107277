<?php

namespace App\Bitm\SEIP118719\Profile_Picture;
use \App\Bitm\SEIP118719\Utility\Utility;

class Profile_Picture {
    //public $serial_no="";
    public $id = "";
	public $title = "";
	public $picture = "";
	public $temp = "";
    //public $created = "";
    ///public $modified = "";
    // public $created_by = "";
    // public $modified_by = "";
    // public $deleted_at = ""; //soft delete
    
    
    //Start Construct function. It will generate when we will create an object.
    public function __construct($data = false){
        
        if( is_array($data) && array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        
			@$this->title = $data["name"];
			@$this->images = $_FILES["avatar"]["name"];
			@$this->temp = $_FILES["avatar"]["tmp_name"];
    }
    
     //Start Show/View Function
        public function show($id=false){
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");

        $query = "SELECT * FROM `tbl_profile_pictures` WHERE id =" . $id;
        $result = mysql_query($query);

        $row = mysql_fetch_assoc($result);

        return $row;
    }
    //End Show/View Function
    
    //Start Index Function
    public function index(){
        $avators = array();
				
				$connection = mysql_connect("localhost","root","") or die("mySQL is not connected successfully.");
				
				$link = mysql_select_db("db_atomic_project_suraiya") or die("Database is not selected.");
				
				$quary = "SELECT * FROM `tbl_profile_pictures`";
				
				$result = mysql_query($quary);
				
				while($row = mysql_fetch_assoc($result)){
					$avators[] = $row;
				}
				return $avators;
    }
    //End Index Function
    
    //Start Store Function
    public function store(){
        $connection = mysql_connect("localhost","root","") or die("mySQL is not connected successfully.");
				
				$link = mysql_select_db("db_atomic_project_suraiya") or die("Database is not selected.");
				
				move_uploaded_file($this->temp, "images/$this->images");
				
				$quary = "INSERT INTO `db_atomic_project_suraiya`.`tbl_profile_pictures` ( `name`, `picture`) VALUES ( '".$this->title."', '".$this->images."')";
				//var_dump($quary) or die();
				$result = mysql_query($quary);
				
				
				Utility::redirect("index.php");
    }
    //End Store Function
  
    //Start Delete Function
    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");

        $query = "DELETE FROM `db_atomic_project_suraiya`.`tbl_profile_pictures` WHERE `tbl_profile_pictures`.`id` = ".$id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Profile Picture is deleted successfully.");
        }else{
            Utility::message(" Cannot delete.");
        }
        
        Utility::redirect('index.php');
    }
    //End Delete Function
    
    //Start Update Function
        public function update(){
            
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");
        
        move_uploaded_file($this->temp, "images/$this->images");
        
       $query ="UPDATE `db_atomic_project_suraiya`.`tbl_profile_pictures` SET `name` = '".$this->title."', `picture` = '".$this->images."' WHERE `tbl_profile_pictures`.`id` = ".$this->id;
        
       //var_dump($query); die();
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Book title is edited successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
     //End Update Function
    
}
