<?php

namespace App\Bitm\SEIP118719\Birthday;
use \App\Bitm\SEIP118719\Utility\Utility;
class Birthday {
    public $serial_no="";
    public $id="";
    public $user_name="";
    public $birth_date="";
    // public $created;
    //public $modified;
    //public $created_by;
    //public $modified_by;
    //public $deleted_at;
    
    //Start Construct function. It will generate when we will create an object.
    public function __construct($data=false){
          
        if( is_array($data) && array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        $this->user_name=$data['user_name'];
        $this->birth_date=$data['birth_date'];
    }
         //Start Show/View Function
        public function show($id=false){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");

        $query = "SELECT * FROM `tbl_birthdays` WHERE id =".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_assoc($result);
        
        return $row;
    }
    //End Show/View Function
    
    //Start Index Function
    public function index(){
        $birth_objs = array();
        $conn=  mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk=  mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");
        $query="SELECT * FROM `tbl_birthdays`";
        $result=  mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $birth_objs[]=$row;
        }
        return $birth_objs;
    }
     //End Index Function
    
//Start Store Function
    public function store(){
        $conn= mysql_connect("localhost","root","") or die("Cannot connect with database.");
        $lnk=  mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");
        $query="INSERT INTO `db_atomic_project_suraiya`.`tbl_birthdays` (`user_name`, `birth_date`) VALUES ('".$this->user_name."', '".$this->birth_date."')";
       
        $result =  mysql_query($query);
          if($result){
        Utility::message("Birthday Date added successfully.");
        }
            else{
               Utility::message("There is an error while saving data, please try again later..."); 
            }
       Utility::redirect('index.php');
    }
     //End Store Function
     //
    //Start Delete Function
    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");

        $query = "DELETE FROM `db_atomic_project_suraiya`.`tbl_birthdays` WHERE `tbl_birthdays`.`id` = ".$id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Birthday Date is deleted successfully.");
        }else{
            Utility::message(" Cannot delete.");
        }
        
        Utility::redirect('index.php');
    }
    //End Delete Function
    
     //Start Update Function
        public function update(){
            
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");
        
        $query = "UPDATE `db_atomic_project_suraiya`.`tbl_birthdays` SET `user_name` = '".$this->user_name."', `birth_date` = '".$this->birth_date."' WHERE `tbl_birthdays`.`id` = ".$this->id;

        $result = mysql_query($query);
               
        if($result){
            Utility::message("Birthday Date is edited successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
     //End Update Function
}