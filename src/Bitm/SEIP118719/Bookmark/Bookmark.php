<?php

namespace App\Bitm\SEIP118719\Bookmark;
use \App\Bitm\SEIP118719\Utility\Utility;

class Bookmark {
    //public $serial_no="";
    public $id="";
    public $book_mark="";
    //public $created = "";
    ///public $modified = "";
    // public $created_by = "";
    // public $modified_by = "";
    // public $deleted_at = ""; //soft delete
    
    
    //Start Construct function. It will generate when we will create an object.
    public function __construct($data = false){
        
        if( is_array($data) && array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        
        $this->book_mark=$data['book_mark'];
    }
    
     //Start Show/View Function
        public function show($id=false){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");

        $query = "SELECT * FROM `tbl_bookmarks` WHERE id =".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_assoc($result);
        
        return $row;
    }
    //End Show/View Function
    
    //Start Index Function
    public function index(){
        $book_mark_objs = array();
        $conn=  mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk=  mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");
        $query="SELECT * FROM `tbl_bookmarks`";
        $result=  mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $book_mark_objs[]=$row;
        }
        return $book_mark_objs;
    }
    //End Index Function
    
    //Start Store Function
    public function store(){
        $conn=  mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk=  mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");
        $query="INSERT INTO `db_atomic_project_suraiya`.`tbl_bookmarks` ( `book_mark`) VALUES ('".$this->book_mark."')";
        $result=  mysql_query($query);
        if($result){
        Utility::message("Book mark url added successfully.");
        }
            else{
               Utility::message("There is an error while saving data, please try again later..."); 
            }
        Utility::redirect('index.php');
    }
    //End Store Function
  
    //Start Delete Function
    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");

        $query = "DELETE FROM `db_atomic_project_suraiya`.`tbl_bookmarks` WHERE `tbl_bookmarks`.`id` = ".$id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Book mark url is deleted successfully.");
        }else{
            Utility::message(" Cannot delete.");
        }
        
        Utility::redirect('index.php');
    }
    //End Delete Function
    
    //Start Update Function
        public function update(){
            
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomic_project_suraiya") or die("Cannot select database.");
        
        $query = "UPDATE `db_atomic_project_suraiya`.`tbl_bookmarks` SET `book_mark` = '".$this->book_mark."' WHERE `tbl_bookmarks`.`id` = ".$this->id;

        $result = mysql_query($query);
               
        if($result){
            Utility::message("Book mark url is edited successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
     //End Update Function
    
}
