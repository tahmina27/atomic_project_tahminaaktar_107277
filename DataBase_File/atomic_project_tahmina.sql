-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2016 at 11:18 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_tahmina`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_birthdays`
--

CREATE TABLE `tbl_birthdays` (
  `serial_no` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `birth_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_birthdays`
--

INSERT INTO `tbl_birthdays` (`serial_no`, `id`, `user_name`, `birth_date`) VALUES
(1, 1, 'Linkon Miyan', '1993-08-20'),
(0, 29, 'Suraiya Aysha Asa', '1992-10-15'),
(0, 30, 'Shafiqul Islam', '1992-10-20'),
(0, 35, 'Shafiqul Islam', '1992-10-15'),
(0, 36, 'Shafiqul Islam', '1993-08-17'),
(0, 38, 'Shafiqul Islam', '0000-00-00'),
(0, 39, 'asa', '2016-01-04'),
(0, 41, 'Suraiya Aysha', '1992-10-15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bookmarks`
--

CREATE TABLE `tbl_bookmarks` (
  `id` int(11) NOT NULL,
  `book_mark` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_bookmarks`
--

INSERT INTO `tbl_bookmarks` (`id`, `book_mark`) VALUES
(1, ' http://www.w3schools.com'),
(2, ' 	https://www.google.com.bd/?gws_rd=cr&ei=jM6UVqOJJ43guQSxxp2QDA'),
(3, 'http://www.w3schools.com/php/'),
(4, 'https://www.google.com.bd/?gws_rd=cr&ei=jM6UVqOJJ43guQSxxp2QDA'),
(5, 'https://about.gitlab.com/gitlab-com/');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_books`
--

CREATE TABLE `tbl_books` (
  `serial_no` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `book_title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_books`
--

INSERT INTO `tbl_books` (`serial_no`, `id`, `book_title`, `author`) VALUES
(1, 1, 'The Clean Code1129', 'Robert C. Martin'),
(0, 2, 'How learn HTML', 'Asa'),
(0, 3, 'How to learn Java', 'Bappi Ashrafi'),
(0, 4, 'How to learn Wordpress', 'Asa'),
(0, 5, 'How to learn Wordpress', 'Asa');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_citys`
--

CREATE TABLE `tbl_citys` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_citys`
--

INSERT INTO `tbl_citys` (`id`, `name`, `city`) VALUES
(11, 'Suraiya', 'Rajshahi'),
(12, 'Linkon Miyan', 'Dhaka'),
(13, 'Tahmina', 'Rangpur'),
(14, 'Suraiya', 'Khulna'),
(15, 'Linkon Miyan', 'Dhaka'),
(20, 'asa', 'Khulna');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_subscriptions`
--

CREATE TABLE `tbl_email_subscriptions` (
  `serial_no` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email_subscription` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_email_subscriptions`
--

INSERT INTO `tbl_email_subscriptions` (`serial_no`, `id`, `first_name`, `last_name`, `email_subscription`) VALUES
(0, 4, 'Linkon', 'Aysha Asa', 'asa@yahoo.com'),
(0, 18, 'Sauraiya', 'Aysha Asa', 'asa@yahoo.com'),
(0, 19, 'Linkon', 'Miyan', 'miyan@gmail.com'),
(0, 20, 'Suraiya', 'Miyan', 'asa@yahoo.com'),
(0, 24, 'Linkonhg', 'Miyan', 'mmmiyan@gmail.com'),
(0, 35, 'Linkon', 'Miyan', 'miyan@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_genders`
--

CREATE TABLE `tbl_genders` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_genders`
--

INSERT INTO `tbl_genders` (`id`, `name`, `gender`) VALUES
(1, 'Suraiya', 'Female'),
(2, 'Linkon Miyan', 'Male'),
(4, 'Suraiya', 'Female'),
(10, 'Suraiya Asa', 'Female'),
(12, 'MAyeen', 'Male'),
(13, 'sony', 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hobbys`
--

CREATE TABLE `tbl_hobbys` (
  `id` int(11) NOT NULL,
  `hobby` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_hobbys`
--

INSERT INTO `tbl_hobbys` (`id`, `hobby`) VALUES
(2, 'Reading,Fishing,Sports'),
(5, 'Reading,Fishing,Sports'),
(6, 'Fishing');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profile_pictures`
--

CREATE TABLE `tbl_profile_pictures` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_profile_pictures`
--

INSERT INTO `tbl_profile_pictures` (`id`, `name`, `picture`) VALUES
(5, 'Tahmina Sharif', 'a-beautiful-day-wallpaper-11.jpg'),
(7, 'aomy', '373001-bigthumbnail.jpg'),
(24, 'Tahmina', 'images 1221.jpg'),
(25, 'Imran', 'Beautiful-Butterflies-butterflies-9482003-1600-1200.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_summary_organizations`
--

CREATE TABLE `tbl_summary_organizations` (
  `serial_no` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `summary_organizations` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_summary_organizations`
--

INSERT INTO `tbl_summary_organizations` (`serial_no`, `id`, `company_name`, `summary_organizations`) VALUES
(1, 1, 'RRF', '  Update your Company Summary.'),
(0, 16, 'BASIS', 'In publishing and graphic design, lorem ipsum (derived from Latin dolorem ipsum, translated as "pain itself") is a filler text commonly used to demonstrate the graphic elements of a document or visual presentation. Replacing meaningful content with placeholder text allows viewers to focus on graphic aspects such as font, typography, and page layout without being distracted by the content.                             \r\n                                    '),
(0, 24, 'Basis', 'Ok. Maybe you wanted to come to this site, maybe not. Well, at least give us a chance to PROVE TO YOU the Bible has to be the Word of God. Please, give us the opportunity to show you three thingsâ€”one is how to be saved according to â€œGodâ€™s Wordâ€; and then what is going on in the world right now; and lastly, what will come to pass shortly. We sell nothing. We                ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tarms`
--

CREATE TABLE `tbl_tarms` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `cond` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_birthdays`
--
ALTER TABLE `tbl_birthdays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_bookmarks`
--
ALTER TABLE `tbl_bookmarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_books`
--
ALTER TABLE `tbl_books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_citys`
--
ALTER TABLE `tbl_citys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_email_subscriptions`
--
ALTER TABLE `tbl_email_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_genders`
--
ALTER TABLE `tbl_genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_hobbys`
--
ALTER TABLE `tbl_hobbys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_profile_pictures`
--
ALTER TABLE `tbl_profile_pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_summary_organizations`
--
ALTER TABLE `tbl_summary_organizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_tarms`
--
ALTER TABLE `tbl_tarms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_birthdays`
--
ALTER TABLE `tbl_birthdays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `tbl_bookmarks`
--
ALTER TABLE `tbl_bookmarks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_books`
--
ALTER TABLE `tbl_books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_citys`
--
ALTER TABLE `tbl_citys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_email_subscriptions`
--
ALTER TABLE `tbl_email_subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tbl_genders`
--
ALTER TABLE `tbl_genders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_hobbys`
--
ALTER TABLE `tbl_hobbys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_profile_pictures`
--
ALTER TABLE `tbl_profile_pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `tbl_summary_organizations`
--
ALTER TABLE `tbl_summary_organizations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tbl_tarms`
--
ALTER TABLE `tbl_tarms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
